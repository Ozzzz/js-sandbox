/**
 * main
 * @author Albert <ozouakimanon@yahoo.fr>
 * Simple approche algorithmique :
 *  - definition de variables
 *  - affectations de valeurs aux variables
 *  - les différents types scalaires
 * @version 1.0.0
 */



let myName = '';

// Affectation de valeur Albert à la variable myName
myName = 'Albert';
yourName = 'Bond';

// Affichage du contenu de la variable myName
console.log(myName); // Expected output : Albert

// Changer la valeur de myName par autre chose
myName = 'Autre chose';
console.log(myName); //Expected output : Autre chose

let isMale = true;
let hasADog = true;

let users = [
    'Aubert',
    'Abdel',
    'Nesibe',
    'Rainui',
    'Albert',
    'Jefferson'
];

// Affecter le contenu d'une variable à une autre variable
myName = yourName;
yourName = 'Superman';
console.log(`${myName} <=> ${yourName}`);

console.log(users);

for (let index = 0; index <= 5; index = index + 1) {
    console.log(users[index]);
}

console.log(' ----------------- boucle TANT QUE ----------------- ');

let indice = 0;
while (indice < users.length) {
    console.log(users[indice]);
    indice = indice + 1;
}

if (users[0] == 'Aubert') {
    console.log(`Salut ${users[0]}`);
}

if (users[3] == 'Aubert') {
    console.log(`Hola ${users[3]}`)
} else {
    console.log(`ce n'est pas Aubert du coup Salut ${users[3]}`);
}

let isTrue = 0;

if (isTrue === false) {
    console.log('Je suis vrai');
} else {
    console.log('Je suis faux');
}

let anyValue = 1;
switch (anyValue) {
    case true:
        console.log('true');
    case 0:
    case 1:
    case 2:
        console.log('je fais partie de 0..2');
    case 3:
        console.log('je suis seulement 3')
        break
    
}