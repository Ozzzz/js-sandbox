/**
 * crypto
 * @author Albert <ozouakimanon@yahoo.fr>
 * cryptage
 * partir d'une chaîne "admin" et obtenir
 * quelque chose du genre ")Dx1z"
 * @version 1.0.0
 */

let tableSign = [

    "(",
    ")",
    "!",
    "/",
    "*"

];

let tableLowercaseLetter = [

    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z"

];

let tableUppercaseLetter = [

    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z"

];

let tableNumber = [

    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9

];


let motACrypter = "admin";

let randomNumberSign = Math.floor(Math.random() * tableSign.length);
let randomNumberLowLetter = Math.floor(Math.random() * tableLowercaseLetter.length);
let randomNumberUpperLetter = Math.floor(Math.random() * tableUppercaseLetter.length);
let randomNumberNumber = Math.floor(Math.random() * tableNumber.length);

let randomSign = tableSign[randomNumberSign];
let randomLowerLetter = tableLowercaseLetter[randomNumberLowLetter];
let randomUpperLetter = tableUppercaseLetter[randomNumberUpperLetter];
let randomNumberFromTable = tableNumber[randomNumberNumber];

let randomTableOf = [randomSign, randomUpperLetter, randomLowerLetter, randomNumberFromTable, randomLowerLetter];



/**
 * cryp(string: String)
 * @param {String} string 
 * @return String
 */

function crypt (string) {

    let newString = string;
    
    for (let index = 0; index < string.length; index++) {
        newString = newString.replace(string[index], randomTableOf[index]);
    }

    return newString;

}

crypt(motACrypter);

function decrypt (string) {
    
    

    return string;
}

