/**
 * abstract collection typescript
 * @author Albert <ozouakimanon@yahoo.fr>
 * typescript initiation part 3
 * @version 1.0.0
 */


export abstract class Collection<T> {
    protected collection: T[] = [];

    public add(item: T): void{
        this.collection.push(item);
    }

    public update(item: T): void{}

    public remove(item: T): void{}

    public size(): number {
        return this.collection.length;
    }

    public all(): T[] {
        return this.collection;
    }
}