/**
 * user collection typescript
 * @author Albert <ozouakimanon@yahoo.fr>
 * typescript initiation part 4
 * @version 1.0.0
 */

import { User } from "../models/user";
import { Collection } from "./collection";

export class UserCollection extends Collection<User> {
    /**
     * @Override
     * @param user
     */
    public add(user: User): void {

        if (this.collection.findIndex((item: User) => item.getId() === user.getId()) === -1) {
            super.add(user);
        }
    }
}