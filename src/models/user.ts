/**
 * user typescript
 * @author Albert <ozouakimanon@yahoo.fr>
 * typescript initiation part 2
 * @version 1.0.0
 */

export class User {
    public lastName!: string;
    public firstName!: string;
    private id!: string;
    private password!: string;

    public setId(id: string): void {
        if (this.id === undefined) {
            this.id = id;
        }
        
    }

    public getId(): string {
        return this.id;
    }

    public setPassword(password: string): void {
        this.password = password;
    }

    public getPassword(): string {
        return this.password;
    }

    public toString(): string {
        return `
            ${this.firstName} ${this.lastName}
            ID: ${this.id}
            Password : You can't see it !
        `;
    }
}