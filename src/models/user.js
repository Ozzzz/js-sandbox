"use strict";
/**
 * user typescript
 * @author Albert <ozouakimanon@yahoo.fr>
 * typescript initiation part 2
 * @version 1.0.0
 */
exports.__esModule = true;
exports.User = void 0;
var User = /** @class */ (function () {
    function User() {
    }
    User.prototype.setId = function (id) {
        if (this.id === undefined) {
            this.id = id;
        }
    };
    User.prototype.getId = function () {
        return this.id;
    };
    User.prototype.setPassword = function (password) {
        this.password = password;
    };
    User.prototype.getPassword = function () {
        return this.password;
    };
    User.prototype.toString = function () {
        return "\n            ".concat(this.firstName, " ").concat(this.lastName, "\n            ID: ").concat(this.id, "\n            Password : You can't see it !\n        ");
    };
    return User;
}());
exports.User = User;
