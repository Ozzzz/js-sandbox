/**
 * user-list
 * @author Albert <ozouakimanon@yahoo.fr>
 * user story 1
 * En tant qu'admin, pouvoir créer une liste avec des utilisateurs
 * ayant des données d'identification
 * @version 1.0.0
 */

class User {

    constructor() {
        this.name = '';
        this.firstName = '';
        this.id = '';
        this.password = '';
    }

}

let listUsers = [];
const user = new User();
user.name = 'OZOUAKI';
user.firstName = 'Albert';
user.id = "aozouaki";
user.password = 'admin';

/**
 * unique(user: User, list: User[])
 * @param user: User une variable de type User
 * @param list: User[] un tableau de User
 * @return boolean
 */

function unique(user, list) {

    let yesYouCan = true;
    
    for (let index = 0; index < list.length; index++) {
        if (list[index].id === user.id) {
            yesYouCan = false;
        }
    }
    
    return yesYouCan;

}

/**
 * ajouter(user: User, list: User[])
 * @param {User} user Some user variable 
 * @param {User[]} list A list of users
 * @return User[]
 */

function ajouter(user, list) {

    if (unique(user, list)) {
        list.push(user);
    }

    return list;

}


listUsers = ajouter(user, listUsers);
listUsers = ajouter(user, listUsers);


//Pseudo test : expect listUsers got 1 item

console.log(`Items : ${listUsers.length}`);
