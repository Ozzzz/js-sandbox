/**
 * main typescript
 * @author Albert <ozouakimanon@yahoo.fr>
 * typescript initiation
 * @version 1.0.0
 */

import { createLogicalNot } from "typescript";
import { UserCollection } from "./src/collection/user-collection";
import { User } from "./src/models/user";

class Main {
    public constructor() {
        const userCollection: UserCollection = new UserCollection();

        const user: User = new User();
        user.lastName = 'OZOUAKI';
        user.firstName = 'Albert';
        user.setId('aozouaki');
        user.setPassword('admin');
        console.log(user.toString());

        userCollection.add(user);
        userCollection.add(user);
        
        console.log(`Collection contains : ${userCollection.size()} item(s)`);

        const password: string = 'titi';

        const login: Login = new Login();
        login.setCredentials(id, password);
        const result = login.process();
        if (result) {
            console.log("Okay tu peux");
        } else {
            console.log("Unauthorized access");
        }
    }
}

//Load Main
new Main();