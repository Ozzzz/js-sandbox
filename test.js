/**
 * test
 * @author Albert <ozouakimanon@yahoo.fr>
 * Exercices
 * @version 1.0.0
 */


let userAges = [
    [
        'Jean-Luc', 53
    ],
    [
        'Bond', 35
    ],
    [
        'Joker', 75
    ]
];

// 1. Afficher la liste des utilisateurs avec leur âge
// Jean-Luc 53
// Bond 35
// Joker 75

// 2. Dites bonjour aux utilisateurs de moins de 50 ans
// Bonjour Bond

// 3. Calculer l'âge moyen des utilisateurs
// Attendu 54.333333333


console.log('------ 1. ------')

let user1 = userAges[0];
let user2 = userAges[1];
let user3 = userAges[2];

console.log(`${user1[0]} ${user1[1]}`);
console.log(`${user2[0]} ${user2[1]}`);
console.log(`${user3[0]} ${user3[1]}`);


console.log('------ 2. ------')

if (user1[1] < 50) {
    console.log(`Bonjour ${user1[0]}`)
};

if (user2[1] < 50) {
    console.log(`Bonjour ${user2[0]}`)
};

if (user3[1] < 50) {
    console.log(`Bonjour ${user3[0]}`)
};


console.log('------ 3. ------')

let sommeAges = user1[1] + user2[1] + user3[1];

console.log(`somme des ages : ${sommeAges}`);

let moyenneAge = sommeAges / userAges.length;

console.log(`age moyen : ${moyenneAge}`);